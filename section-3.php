<section class="portfolio-section my-5 p-0 px-1 portfolio-box"> 
		
    <div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="mb-5 mt-3 col-12 text-center text-destaq">Galeria de Produtos</h2>
				<span class="line-yellow my-2"></span>
			</div>
		</div>
		<p class="text-center">Veja alguns modelos de nossos melhores produtos</p>
	</div>
	<?php
            $palavraEstudo_s8 = array(            
            'sacos plásticos transparentes',
            'fabrica de sacos plásticos',
            'saco personalizado',
            'saco plástico com lacre',           
            'sacos plásticos para embalagem',
            'plástico adesivo transparente',
            'saco plástico zip',
            'sacos plásticos personalizados'
            );
                
            include 'inc/vetKey.php';            
            asort($vetKey); ?>
    
            <div class='portfolio-box container gallery'><div class='row'>
    
            <?php foreach ($vetKey as $key => $value) {
            if(in_array(strtolower($value['key']), $palavraEstudo_s8)){  
                
                $arquivojpg=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-1.jpg";
                $arquivojpg0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-01.jpg";
                $arquivopng=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-1.png";
                $arquivopng0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-01.png";

                if (file_exists($arquivojpg)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-1.jpg"; 
                } else
                if (file_exists($arquivojpg0)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-01.jpg";
                } else
                if (file_exists($arquivopng)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-1.png";
                } else
                if (file_exists($arquivopng0)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-01.png";
                } else {
                    $imagem="assets/img/logo-ok.png";                        
                } ?>
                
					<div class='col-md-3' style='padding:15px;'>
                        <div class='project-post'>
						
							<img src='<?=$url?><?=$imagem?>' alt='<?=$value['key'];?>' title='<?=$value['key'];?>' style='width:100%;'>
                            
									<div class='project-content text-center d-flex align-items-center justify-content-center' style='height:60px'>
						              <h2 class='m-0' style='font-size:13px;line-height: 20px;'><?=$value['key'];?></h2>                                        
                                    </div>
							     <div class='hover-box' style='border-radius:0;padding:10px'>
                                     <div class="col-12 p-0 h-100 borderhover">
				                    <a href='<?=$imagem;?>' data-caption='<?=$value['key'];?>' data-fancybox='group2' class='lightbox zoom' >
                                        <i class='fa fa-eye'></i>
                                     </a>
                                     </div>
				                </div>
                        
				        </div>
                    </div>
					
			<?php	} } ?>
        </div>
    </div>
  
</section>
