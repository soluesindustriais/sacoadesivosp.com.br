<header>
  <nav id="menu" class="navbar navbar-expand-md navbar-light ">
    <div class="container">
      <a class="navbar-brand" href="<?=$url?>" title="<?=$nomeSite." - ".$slogan?>"><img src="<?=$url?>assets/img/logo.png" alt="<?=$nomeSite." - ".$slogan?>" title="<?=$nomeSite." - ".$slogan?>"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar1" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse menu-fix" id="navbar1">
        <ul class="navbar-nav ml-auto h-100 ">
          <?php include 'inc/menu.php' ?>
        </ul>
      </div>
    </div>
  </nav>
</header>