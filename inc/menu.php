<li class="nav-item">
	<a class="nav-link menu-text" href="<?=$url?>" title="Página inicial">Início <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
	<a class="nav-link menu-text" href="<?=$url?>quem-somos" title="Quem Somos">Quem Somos</a>
</li>
<li class="nav-item dropdown">
	<a class="nav-link menu-text dropdown-toggle"  data-hover="dropdown" href="<?=$url?>informacoes" title="Informações" id="informacoes">Informações</a>
	<ul class="dropdown-menu second-lvl" style="padding-bottom:0px;padding-top:0px;">	
		<?php
		include 'inc/sub-menu.php'
		?>
		
		<li class="col-12" style="border-top:1px solid rgba(0,0,0,.3);padding:0px;"><a class="dropdown-item drop-dwns w-100" style="padding:5px;" href="<?=$url?>informacoes" title="Informações">Ver todos</a></li>
	</ul>
</li>
<li class="nav-item">
	<a class="nav-link menu-text" href="<?=$url?>blog" title="Blog">Blog</a>
</li>