<?php if (isset($_POST['qualDesc'])) {  echo $desc; }   include('inc/geral.php'); ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="<?=$url?>assets/img/favicon.png">
<title><?=$title." - ".$nomeSite?></title>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">-->
<link href="<?=$url?>assets/css/bootstrap.min.css" rel="stylesheet">

<?php  if ($h1 == "Informações"){?>
<link rel="stylesheet" href="<?=$url?>assets/js/jquery.paginate.css">
<?php } ?>
<?php if ($title == "Início") { ?>
<link rel="stylesheet" href="<?=$url?>assets/css/owl.carousel.min.css">
            <link rel="stylesheet" href="<?=$url?>assets/css/owl.theme.default.min.css">
        <?php } ?>
<link href="<?=$url?>assets/css/style.css" rel="stylesheet">
<base href="<?=$url;?>">
<meta name="description" content="<?=ucfirst($desc)?>">
<meta name="keywords" content="<?=str_replace($prepos,', ', $h1).', '.$nomeSite?>">
<meta name="geo.position" content="<?=$latitude.";".$longitude?>">
<meta name="geo.placename" content="<?=$cidade."-".$uf?>">
<meta name="geo.region" content="<?=$uf?>-BR">
<meta name="ICBM" content="<?=$latitude.";".$longitude?>">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="<?=$url.$urlPagina?>">
<meta name="author" content="<?=$nomeSite?>">
<link rel="shortcut icon" href="<?=$url?>assets/img/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="<?=$title." - ".$nomeSite?>">
<meta property="og:type" content="article">
<meta property="og:image" content="<?=$url?>assets/img/logo.png">
<meta property="og:url" content="<?=$url.$urlPagina?>">
<meta property="og:description" content="<?=$desc?>">
<meta property="og:site_name" content="<?=$nomeSite?>">  