<footer class="footer-bs" style="padding:30px 0px;padding-bottom:0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 footer-brand">
                <address>
                    <p class="text-white"><?=$nomeSite." - ".$slogan?></p>                    
                </address>                
            </div>
            <div class="col-md-6 col-12 footer-nav" >
                
                    <div class=" fixmenubt flex-wrap">
                        <div class="row w-100 justify-content-end">
                            
                    <ul class="pages col-12 justify-content-end">
                        <li class="d-flex text-center mx-md-3 mx-sm-0  mb-2"><a rel="nofollow" title="Início" href="<?=$url?>">Início</a></li>
                        
                        <?php
                        
                        $pages = array();
                        $pages[0] = array("url" => "quem-somos" , "title" => "Quem Somos");
                        $pages[1] = array("url" => "Informações" , "title" => "Informações");
                        // $pages[2] = array("title" => "Contato");
                 
                        foreach ($pages as $key => $value) {
                        echo "<li class=\"d-flex text-center mx-md-3 mx-sm-12 mb-2\"><a rel=\"nofollow\" title=\"".$value['title']."\" href=\"".$url.titletourl($value['url'])."\">".$value['title']."</a></li>";
                        }
                        ?>
                        <li><a title="Mapa do site" href="<?=$url?>mapa-site">Mapa do site</a></li>
                    </ul>
                            
                </div>
                </div>
             
            </div>
          
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">   
         <div class="copyright-footer text-white col-12 justify-content-between d-flex align-items-center" style="border-top:1px solid #555;padding:0px;">
        <div class="container">
            <div class="row">
                
            
              <p class="mb-0 col-md-6 col-6">   
                Copyright © <?=$nomeSite?>. (Lei 9610 de 19/02/1998)
              </p>
            <div class="selos col-md-6 col-6">
                <a rel="nofollow" href="http://validator.w3.org/check?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" target="_blank" title="HTML5 W3C" style="margin-right:10px"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
                <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C" ><i class="fab fa-css3"></i> <strong>W3C</strong></a>
                
           
        </div>
        </div>
          <div class="row pb-2">
                <div class="logo_footer text-white w-100 text-center" style="font-size:13px">
                    <img src="<?=$url?>assets/img/logo.png" alt="<?=$nomeSite." - ".$slogan?>" title="<?=$nomeSite." - ".$slogan?>" class="mr-1">
                        é um parceiro
                    <img src="<?=$url?>assets/img/logo-solucs.png" alt="<?=$nomeSite." - ".$slogan?>" title="<?=$nomeSite." - ".$slogan?>" class="ml-1">
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
    
    
  
 
</footer>
<div id="stop" class="scrollTop">
    <span><a href=""><i class="fa fa-chevron-up" aria-hidden="true"></i></a></span>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


<script >
$(document).ready(function() {
    $('.lightbox').fancybox();
});
</script>
<link rel="stylesheet" type="text/css" href="<?=$url?>assets/css/jquery.fancybox.min.css">
<script src="<?=$url?>assets/js/jquery.fancybox.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/solid.css" integrity="sha384-ioUrHig76ITq4aEJ67dHzTvqjsAP/7IzgwE7lgJcg2r7BRNGYSK0LwSmROzYtgzs" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/brands.css" integrity="sha384-i2PyM6FMpVnxjRPi0KW/xIS7hkeSznkllv+Hx/MtYDaHA5VcF0yL3KVlvzp8bWjQ" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/fontawesome.css" integrity="sha384-sri+NftO+0hcisDKgr287Y/1LVnInHJ1l+XC7+FOabmTTIK0HnE2ID+xxvJ21c5J" crossorigin="anonymous">

<script>
    var url = window.location;
    $(function() {
        $('header nav ul li a[href="' + url + '"]').addClass('active');
        $('aside li a[href="' + url + '"]').addClass('active-menu-aside');
    });
</script>

<?php if ($title == "Início") { ?>
<script src="<?=$url?>assets/js/owl.carousel.min.js"></script>

    <script>

            $('.owl-carousel').owlCarousel({
                autoplay: true,
                loop: true,
                margin: 0,
                dots: true,                
                nav: false,
                autoplayHoverPause:true,
                responsiveClass: true,
                lazyLoad: true,
                responsive: {
                  0: {
                    items: 1,                    
                  },
                  600: {
                    items: 3,                   
                  },
                  1000: {
                    items: 4,                                  
                
                  }
                }
              });
    </script>
  
<?php }  ?>


<script>
$(document).ready(function() { 
    var scrollTop = $(".scrollTop");
    $(window).scroll(function() { 
        var topPos = $(this).scrollTop();   
        if (topPos > screen.height-100) { 
            
            $(scrollTop).css("opacity", "1");
        } else {
            $(scrollTop).css("opacity", "0");
        }
    });
    $(scrollTop).click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});
</script>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-MMLLJMEGB7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-MMLLJMEGB7');
</script>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>



<?php if ($h1 == "Informações"){?>
    <script src="<?=$url?>assets/js/jquery.paginate.js"></script>
    <script>
        $('#example').paginate({perPage: 20});
    </script>
<?php } ?> 


<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>

    <!-- Script Launch start -->
    <script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
    <script>
        const aside = document.querySelector('aside');
        const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
        aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
    </script>
    <!-- Script Launch end -->
<?php include 'inc/fancy.php'; ?><div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>