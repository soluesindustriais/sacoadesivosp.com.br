<div class="row d-flex justify-content-center">
    
    <?
	for($i = 1; $i <= $quantia; $i++){
	    $arquivojpg=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$urlPagina."-".$i.".jpg";
        $arquivojpg0=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$urlPagina."-0".$i.".jpg";
        $arquivopng=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$urlPagina."-".$i.".png";
        $arquivopng0=dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$urlPagina."-0".$i.".png";

        if (file_exists($arquivojpg)) {
            $imagem="assets/img/img-mpi/".$urlPagina."-".$i.".jpg"; 
        } else
        if (file_exists($arquivojpg0)) {
            $imagem="assets/img/img-mpi/".$urlPagina."-0".$i.".jpg";
        } else
        if (file_exists($arquivopng)) {
            $imagem="assets/img/img-mpi/".$urlPagina."-".$i.".png";
        } else
        if (file_exists($arquivopng0)) {
            $imagem="assets/img/img-mpi/".$urlPagina."-0".$i.".png";
        } else {
            $imagem="assets/img/logo-ok.png";                        
        }
   ?>


    <div class="col-10 col-md-4" style="padding:5px">
        
            <a href="<?= $imagem; ?>" data-fancybox="group1" class="lightbox" title="<?= $h1; ?>" data-caption="<?= $h1; ?>"> 
                <img src="<?=$url?><?=$imagem?>" style="width:100%;padding:5px;height:250px;border:1px solid #eee;" alt="<?= $h1; ?>" />
            </a>
        
    </div>
        
	
	<?php } ?>

</div>