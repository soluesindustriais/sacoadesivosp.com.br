<?php
$h1 = "Página não encontrada!";
$title = $h1;
$desc = "Desculpe, mas não conseguimos encontrar a página que você estava procurando. É provavelmente alguma coisa que fizemos de errado, mas agora sabemos sobre isso e vamos tentar corrigi-lo";
?>
<!DOCTYPE html>

<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
</head>
<body>

    <style>.R404{font-size: 130px;    font-weight: 700;    color: #444;    text-align: center;    margin: 0;}</style>
    <?php include 'inc/header.php' ?>
  
    <main>
        <section class="container mb-5">
            <div class="row mb-5 pb-5">
                <div class="col-md-12">
                    <?php include 'inc/breadcrumb.php' ?>                  
                </div>
                <div class="col-md-12 text-center">
                    <h2 class="R404">404</h2>
                    <p class="h1">Página não encontrada!</p>
                    <p>Desculpe, mas não conseguimos encontrar a página que você estava procurando. É provavelmente alguma coisa que fizemos de errado, mas agora sabemos sobre isso e vamos tentar corrigi-lo. Entretanto, experimente uma destas opções:</p>
                </div>
                <div class="col-md-6 text-center" style="padding:15px;">
                    <a href="<?= $url; ?>" class="button-slider2" >Voltar a página inicial</a>
                </div>
                <div class="col-md-6 text-center" style="padding:15px;">
                    <a href="<?= $url; ?>mapa-site" class="button-slider2" >Ver o mapa do Site</a>
                </div>
            </div>
        </section>
    </main>
    <?php include 'inc/footer.php' ?>
    </body>
</html>
