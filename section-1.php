<section class="px-1 my-5">
	
    <div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="mb-5 mt-3 col-12 text-center text-destaq">Conheça nossos produtos</h2>
				<span class="line-yellow my-2"></span>
			</div>
		</div>
		<p class="text-center">Faça cotação de embalagens de maneira rápida e fácil. Trabalhamos com diversos fornecedores espalhados pelo Brasil.</p>
	</div>

	<div class="card-deck m-0 mt-3">
		<div class="container">
			<div class="justify-content-around owl-carousel">
            <?php  
                $palavraEstudo_s7 = array(
                'sacolas personalizadas',
                'sacolas plásticas',
                'sacolas plásticas personalizadas',
                'fabrica de sacolas plásticas',
                'sacola de plástico',
                'fabrica de sacolas',
                'sacolas plásticas atacado'
            );
                
            include 'inc/vetKey.php';            
            asort($vetKey);
            foreach ($vetKey as $key => $value) {
                
            if(in_array(strtolower($value['key']), $palavraEstudo_s7)){
                
                $arquivojpg=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-1.jpg";
                $arquivojpg0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-01.jpg";
                $arquivopng=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-1.png";
                $arquivopng0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$value['url']."-01.png";

                if (file_exists($arquivojpg)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-1.jpg"; 
                } else
                if (file_exists($arquivojpg0)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-01.jpg";
                } else
                if (file_exists($arquivopng)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-1.png";
                } else
                if (file_exists($arquivopng0)) {
                    $imagem="assets/img/img-mpi/".$value['url']."-01.png";
                } else {
                    $imagem="assets/img/logo-ok.png";                        
                } 
 
				echo "<div class='col-12 p-0' style=''>
						<a href=\"".$url.$value["url"]."\" title=\"".$value['key']."\">
                                <div class=\" cards border\">
                                    <img class=\"card-img-top new-card \" src=\"".$url."".$imagem."\" alt=\"".$value['key']."\" title=\"".$value['key']."\">
                                    
                                    <div class=\"card-footer \">
                                    <h2 style='height:35px'>".$value['key']."</h2>
                                    <p  class=\"text-card m-0 p-0\" >Saiba mais sobre ".$value['key']."...</p>
                                </div>
                            </div>
                            </a>
                        </div>";
				
                } } ?>
			</div>
		</div>
	</div>
</section>