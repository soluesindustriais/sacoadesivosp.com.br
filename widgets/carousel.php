<div id="carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators lineSlide">
		<li data-target="#carousel" data-slide-to="0" class="active"></li>
		<li data-target="#carousel" data-slide-to="1"></li>
		<li data-target="#carousel" data-slide-to="2"></li>
	</ol>

	<div class="carousel-inner">
		<div class="carousel-item active one">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo" style="">Sacolas Plásticas</h2>
				<p class="sdesc" style="">Hoje em dia o mercado das embalagens atento cada vez mais as necessidades dos clientes, oferece uma variedade de embalagens...</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."sacolas-plasticas.php")) { echo $url."sacolas-plasticas"; } ?>" class="btn button-slider2">Saiba Mais</a>
                </div>
			</div>
		</div>	
		<div class="carousel-item three">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo"  style="">Sacolas Plásticas Personalizadas</h2>
				<p class="sdesc" style="">A utilização das mais diferentes embalagens, faz com que muitas empresas atentas a essa necessidade produzam sacolas plásticas personalizadas...</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."sacolas-plasticas-personalizadas.php")) { echo $url."sacolas-plasticas-personalizadas"; } ?>" class="btn button-slider2">Saiba Mais</a>
			</div>
            </div>
		</div>
        <div class="carousel-item two">
			<div class="d-flex justify-content-center align-items-center scont">
                <div class="col-md-9 col-11 text-center" style="margin-top:-50px">
				<h2 class="stitulo"  style="">Sacolas Plásticas Atacado</h2>
				<p class="sdesc"  style="">A utilização de sacolas plásticas atacado é muito comum e frequente em todos os lugares, isso porque esse tipo de embalagem é...</p>
				<a href="<?php if (file_exists(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."sacolas-plasticas-atacado.php")) { echo $url."sacolas-plasticas-atacado"; } ?>" class="btn button-slider2">Saiba Mais</a>
			</div>
            </div>
		</div>
	</div>

	<a class="carousel-control-prev " href="#carousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>

	<a class="carousel-control-next " href="#carousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon seta" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>

</div>

