<?php
$h1 = "Informações";
$title = "informacoes";
$desc = "Faça cotação dos sacos zip de forma prática. Tenha acesso a uma ampla rede de fornecedores e garanta segurança para o seu produto por um preço que cabe no seu bolso"; 
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php include('inc/head.php'); ?>
</head>

<body>
    <?php include 'inc/header.php' ?>

    <section class="container mb-5 informacoes portfolio-section portfolio-box">
        <div class="row m-0">
            <div class="col-md-12">
               
                    <?php
              include_once('inc/vetKey.php');
                echo "<div class='portfolio-box container'><div class='row' id='example'>";
                foreach ($vetKey as $key => $vetor) {
                    
                    $arquivojpg=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$vetor['url']."-1.jpg";
                    $arquivojpg0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$vetor['url']."-01.jpg";
                    $arquivopng=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$vetor['url']."-1.png";
                    $arquivopng0=dirname(__FILE__).DIRECTORY_SEPARATOR."assets/img/img-mpi".DIRECTORY_SEPARATOR.$vetor['url']."-01.png";
                           
                    if (file_exists($arquivojpg)) {
                        $imagem="assets/img/img-mpi/".$vetor['url']."-1.jpg"; 
                    } else
                    if (file_exists($arquivojpg0)) {
                        $imagem="assets/img/img-mpi/".$vetor['url']."-01.jpg";
                    } else
                    if (file_exists($arquivopng)) {
                        $imagem="assets/img/img-mpi/".$vetor['url']."-1.png";
                    } else
                    if (file_exists($arquivopng0)) {
                        $imagem="assets/img/img-mpi/".$vetor['url']."-01.png";
                    } else {
                        $imagem="assets/img/logo-ok.png";                        
                    }
                    
				echo "<div class='col-md-3' style='padding:15px;'><div class='project-post'>

				<img src='".$url."".$imagem."' alt='".$vetor['key']."' title='".$vetor['key']."' style='width:100%;'>

				<div class='project-content text-center d-flex align-items-center justify-content-center' style='height:60px'>
						              <h2 class='m-0' style='font-size:13px;line-height: 20px;'>".$vetor['key']."</h2>
						
				</div>

				<div class='hover-box' style='border-radius:0;padding:10px'>
                                     <div class='col-12 p-0 h-100 borderhover'>

				<a href='".$vetor['url']."' class='zoom'><i class='fa fa-eye'></i></a>

				</div></div>

				</div></div>";

				

				}
                echo "</div></div>";
                
            ?>

                </div>



      
        </div>
    </section>

    <?php include 'inc/footer.php' ?>
</body>

</html>
