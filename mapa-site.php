<?php
$h1 = "Mapa do site";
$title = $h1;
$desc = "Use o mapa do site para encontrar o que você procura!";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php include('inc/head.php'); ?>
</head>

<body>
    <?php include 'inc/header.php' ?>

		<div class="container mb-5">

			<div class="row">

				<div class="col-md-12">

					<?php include 'inc/breadcrumb.php' ?>
					

				</div>

				<div class="col-md-12">

					
					<div class="d-flex flex-wrap mapasite">

						<?php include 'inc/sub-menu-mapasite.php';?>

					</div>

				</div>

			</div>

		</div>

	<?php include 'inc/footer.php' ?>
    </body>
</html>