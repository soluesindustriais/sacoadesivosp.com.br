<?php
// Caminho para o arquivo ZIP e diretório de instalação
$zipFile = __DIR__ . '/doutor_files.zip';
$installDir = __DIR__; // Diretório onde o instalador está localizado

// 1 - Deletar a pasta "api" se existir
$apiDir = $installDir . '/doutor/api';
if (is_dir($apiDir)) {
    // Função para deletar um diretório recursivamente
    function deleteDir($dirPath) {
        if (!is_dir($dirPath)) return;
        foreach (scandir($dirPath) as $item) {
            if ($item == '.' || $item == '..') continue;
            $itemPath = $dirPath . DIRECTORY_SEPARATOR . $item;
            is_dir($itemPath) ? deleteDir($itemPath) : unlink($itemPath);
        }
        rmdir($dirPath);
    }
    deleteDir($apiDir);
}

// 2 - Extrair o arquivo ZIP para um diretório temporário
$zip = new ZipArchive;
if ($zip->open($zipFile) === TRUE) {
    $tempDir = $installDir . '/temp';
    if (!is_dir($tempDir)) mkdir($tempDir);
    $zip->extractTo($tempDir);
    $zip->close();
} else {
    die("Falha ao abrir o arquivo ZIP.");
}

// 3 - Substituir arquivos conforme especificado

// Caminhos dos arquivos a serem substituídos (com a subpasta 'doutor_files')
$replaceFiles = [
    ['src' => '/doutor_files/functions.php', 'dest' => '/doutor/_cdn/ajax/functions.php'],
    ['src' => '/doutor_files/Categorias.class.php', 'dest' => '/doutor/_app/Models/Categoria.class.php'],
    ['src' => '/doutor_files/Upload.class.php', 'dest' => '/doutor/_app/Helpers/Upload.class.php'],
    ['src' => '/doutor_files/sitemap.php', 'dest' => '/sitemap.php']
];

foreach ($replaceFiles as $file) {
    $source = $tempDir . $file['src'];
    $destination = $installDir . $file['dest'];
    if (file_exists($source)) {
        copy($source, $destination);
    } else {
        echo "Arquivo não encontrado: $source\n";
    }
}

// 4 - Criar nova pasta "api" dentro de "doutor"
$newApiDir = $installDir . '/doutor/api';
if (!is_dir($newApiDir)) mkdir($newApiDir);

// 5 - Mover arquivos para a nova pasta "api"
$apiFiles = [
    '/doutor_files/update.php',
    '/doutor_files/post.php',
    '/doutor_files/get.php',
    '/doutor_files/delete.php'
];
foreach ($apiFiles as $file) {
    $source = $tempDir . $file;
    $destination = $newApiDir . '/' . basename($file); // Coloca os arquivos dentro de 'api'
    if (file_exists($source)) {
        copy($source, $destination);
    } else {
        echo "Arquivo não encontrado: $source\n";
    }
}

// 6 - [REMOVIDO]

// 7 - Modificar arquivos existentes

// Adicionar código ao final de index.php (Etapa 11)
$indexPath = $installDir . '/index.php';
$codeToAdd = <<<PHP
/*Gerador de Sitemap*/

\$fileName = "./last_run_sitemap.txt";
\$updateSitemap = 5;

if (!file_exists(\$fileName)) {
  include("sitemap.php");
}
if (file_exists(\$fileName)) {
 
  \$last_run = fopen(\$fileName, "r");
  \$last_run_day = fread(\$last_run, filesize(\$fileName));
  fclose(\$last_run);

  \$diff = date_diff(new DateTime(\$last_run_day), new DateTime(date('Y-m-d')));

  if (\$diff->days >= \$updateSitemap) {
    include("sitemap.php");
    \$last_run = fopen(\$fileName, "w+");
    fwrite(\$last_run, date('Y-m-d'));
    fclose(\$last_run);
  }
} 

/*Gerador de Sitemap*/
PHP;
file_put_contents($indexPath, $codeToAdd, FILE_APPEND);

// **Remoção da inclusão de CSS foi realizada, então não há mais essa parte.**

// **Remoção de tudo relacionado ao WhatsApp:**
// Remover a modificação do footer-blog.php
$footerPath = $installDir . '/inc/footer-blog.php';
if (file_exists($footerPath)) {
    $footerContent = file_get_contents($footerPath);
    
    // Remover código antigo específico relacionado ao WhatsApp
    $footerContent = str_replace("<?php if (isset(\$whatsapp) && !empty(\$whatsapp)) { include 'whatsapp-button.php'; } ?>", "", $footerContent);
    
    // Remover a inclusão de 'whatsAppBusca.php' caso exista
    $footerContent = str_replace("<?php include('whatsAppBusca.php'); ?>", "", $footerContent);
    
    file_put_contents($footerPath, $footerContent);
}

// 8 - Limpeza do diretório temporário
deleteDir($tempDir);

echo "Instalação concluída com sucesso.";
?>
