<?php

$h1 = "Sacos Zip Lock";
$title = "Início";
$desc = "Realize a cotação de embalagens de maneira rápida e fácil, obtenha contato de diversos fornecedores e escolha o preço que melhor te atenda."; ?>

<!DOCTYPE html>

<html lang="pt-br">
<head>
    <?php include('inc/head.php'); ?>
</head>
<body>
<h1 class="d-none"><?= $h1." - ".$nomeSite; ?></h1>
<?php include 'inc/header.php' ?>
<?php include 'widgets/carousel.php' ?>
<main>
	<?php include('section-1.php'); ?>
	<?php include('section-2.php'); ?>
	<?php include('section-3.php'); ?> 
</main>
<?php include 'inc/footer.php' ?>
</body>
</html>